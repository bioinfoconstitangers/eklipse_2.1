
import json
import os
import sys
import pybam
#from string import split
from eKLIPse_fct import *


#=================================================================================================================================================#
#=================================================================================================================================================#
#=================================================================================================================================================#

def Read_alignment(titleBam,dicoInit,lstError):
    try:
        pathSCjson = os.path.join(dicoInit['pathTmpDir'],titleBam+"_SC.json")
        pathSCfasta = os.path.join(dicoInit['pathTmpDir'],titleBam+".fasta")
        FASTA = open(pathSCfasta,'w')
        # Init dicoBam
        dicoBam = {}
        for pos in range(1,dicoInit["dicoGbk"]['refLength']+1,1): 
            dicoBam[pos] = { 'nb_reads_F':0, 'nb_reads_R':0,\
                             'nb_sc_reads_F':0, 'nb_sc_reads_R':0,\
                             'nb_sc_fasta_F':0, 'nb_sc_fasta_R':0 }
        #***** BROWSE READS & SEARCH SCR *****#
        # Switch to downsampled BAM if exist
        if dicoInit['dicoBam'][titleBam]['path_downsampling']!="" : dicoInit['dicoBam'][titleBam]['path'] = dicoInit['dicoBam'][titleBam]['path_downsampling']
        for alignment in pybam.read(dicoInit['dicoBam'][titleBam]['path']):
            if alignment.file_chromosomes[alignment.sam_refID]==dicoInit["dicoBam"][titleBam]['refName'] and alignment.sam_mapq>=dicoInit['minQ'] and not alignment.sam_cigar_string.__contains__("H") and len(alignment.sam_seq)>=dicoInit['minlen']:# and not explain_sam_flags(alignment.sam_flag).__contains__("second in pair") and not explain_sam_flags(alignment.sam_flag).__contains__("supplementary"):
                #***** RETRIEVE positions tuple & lastMapped infos *****#
                positionsLstTuple,lastMappedPos,lastMappedPosRead = cigar_list_to_tuple(alignment.sam_cigar_list,alignment.sam_pos0)
                #***** FORWARD reads *****#
                if explain_sam_flags(alignment.sam_flag)=="" or explain_sam_flags(alignment.sam_flag).__contains__("mate reverse strand"):
                    # Count reads
                    for posTuple in positionsLstTuple:
                        try : dicoBam[posTuple[1]+1]['nb_reads_F']+=1
                        except: pass # None case
                    # Count softclipped (right soft-clipping)
                    length, operation = alignment.sam_cigar_list[len(alignment.sam_cigar_list)-1]
                    if alignment.sam_qname=="MG1U0:00377:02144": print() 
                    if operation=="S":
                        # dicoBam[lastMappedPos]['nb_sc_reads_F']+=1
                        # Write to Fasta (apply filter) / not consider 'N'
                        if length-alignment.sam_seq[len(alignment.sam_seq)-length:].count("N")>=dicoInit['SCsize'] and lastMappedPos+length<=dicoInit["dicoGbk"]['refLength']:
                            dicoBam[lastMappedPos]['nb_sc_reads_F']+=1
                            nb_mapped_part = min(dicoInit["MappedPart"],lastMappedPosRead)
                            FASTA.write(">"+str(lastMappedPos)+"_"+str(nb_mapped_part)+"_scrF_"+alignment.sam_qname+"\n"+alignment.sam_seq[len(alignment.sam_seq)-length-nb_mapped_part:]+"\n")
                            dicoBam[lastMappedPos]['nb_sc_fasta_F']+=1
                #***** REVERSE reads *****#
                else:
                    # Count reads
                    for posTuple in positionsLstTuple:
                        try : dicoBam[posTuple[1]+1]['nb_reads_R']+=1
                        except: pass
                    # Count softclipped (left soft-clipping)
                    length, operation = alignment.sam_cigar_list[0]
                    if operation=="S":
                        # dicoBam[alignment.sam_pos0+1]['nb_sc_reads_R']+=1
                        # Write to Fasta (apply filter)
                        if length-alignment.sam_seq[0:length].count("N")>=dicoInit['SCsize'] and alignment.sam_pos0+1-length>=0:
                            dicoBam[alignment.sam_pos0+1]['nb_sc_reads_R']+=1
                            nb_mapped_part = min(dicoInit["MappedPart"],length)
                            FASTA.write(">"+str(alignment.sam_pos0+1)+"_"+str(nb_mapped_part)+"_scrR_"+alignment.sam_qname+"\n"+alignment.sam_seq[0:length+nb_mapped_part]+"\n")
                            dicoBam[alignment.sam_pos0+1]['nb_sc_fasta_R']+=1
        # CLOSE files
        FASTA.close()
        # WRITE .json results
        JSON = open(pathSCjson, "w", encoding="utf8")
        JSON.write(json.dumps(dicoBam))
        JSON.close()
    except:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        lstError.append("ReadThread \""+titleBam+"\": "+str(exc_value)+" (line "+str(exc_traceback.tb_lineno)+")")

#=================================================================================================================================================#
#=================================================================================================================================================#
#=================================================================================================================================================#

def SC_blast(titleBam,dicoInit,lstError):
    try:
        pathBLASTjson_del = os.path.join(dicoInit['pathTmpDir'],titleBam+"_delBLAST.json")
        pathBLASTjson_dup = os.path.join(dicoInit['pathTmpDir'],titleBam+"_dupBLAST.json")
        pathSCfasta = os.path.join(dicoInit['pathTmpDir'],titleBam+".fasta")
        pathSCBLAST = os.path.join(dicoInit['pathTmpDir'],titleBam+"_blast.out")
        pathSCjson = os.path.join(dicoInit['pathTmpDir'],titleBam+"_SC.json")
        dicoDel = {} 
        dicoDup = {} 
        # Require sc json files to modify positionsRef
        dicoBam = load_json(pathSCjson)

        #***** LAUNCH BlastN *****# (if fasta not empty)
        if os.path.getsize(pathSCfasta)!=0:
            cmd = dicoInit['pathBlastN']+" -task blastn-short -query "+pathSCfasta+" -db "+os.path.join(dicoInit['pathTmpDir'],"ref.fa")+" -outfmt \"10 qseqid qstart qend sstart send qlen length\" -out "+pathSCBLAST+" -perc_identity "+str(dicoInit['blastIdThreshold'])+" -max_hsps 1 -gapopen "+str(dicoInit['blastGapOpen'])+" -gapextend "+str(dicoInit['blastGapExt'])
            os.system(cmd)
            #***** Read Blast output file *****#
            IN = open(pathSCBLAST,'r')
            lst_lines = IN.read().split("\n")
            IN.close()
            for line in lst_lines:
                if line!="":
                    # Blast line parser
                    splitLine = line.split(",")
                    splitQread = splitLine[0].split("_")
                    SCposRead = int(splitQread[0])
                    SCmappedpart = int(splitQread[1])
                    readOrient = splitQread[2]
                    qstart = int(splitLine[1])
                    qend = int(splitLine[2])
                    sstart = int(splitLine[3])
                    send = int(splitLine[4])
                    qlen = int(splitLine[5])
                    alignLength = int(splitLine[6])
                    covPercent = min((float(alignLength+SCmappedpart)*100.0)/float(qlen),100.0)
                    # Find correspunding positions
                    if covPercent>=dicoInit['blastCovThreshold'] and sstart<send:
                        if readOrient=="scrF":
                            Start = min(SCposRead-SCmappedpart+qstart-1,dicoInit["dicoGbk"]['refLength']-1)
                            End = min(sstart,dicoInit["dicoGbk"]['refLength']-1)
                            Name = str(Start)+"-"+str(End) 
                            # Filter deletions & duplications
                            if (Start<End and End-Start>=dicoInit['SCsize']) or (Start>End and Start-End>=dicoInit["mitosize"]):
                                # Determine if deletion include oriH or oriL
                                if Start>End : rangeDel = [range(Start,16570), *range(1,End+1)]
                                else: rangeDel = range(Start,End+1)
                                intersec_oriH = list(set(dicoInit['range_oriH']) & set(rangeDel))
                                intersec_oriL = list(set(dicoInit['range_oriL']) & set(rangeDel))
                                # check exclude positions
                                boolExcludePosFind = False
                                for excludeRange in dicoInit['exclude_Breakpoint']:
                                    if Start in excludeRange or End in excludeRange or Start+dicoInit['deldupShift'] in excludeRange or End-dicoInit['deldupShift'] in excludeRange:
                                        boolExcludePosFind = True
                                        break
                                # duplication
                                if boolExcludePosFind==False: 
                                    if len(intersec_oriH)>0 or len(intersec_oriL)>0:
                                        if Name in dicoDup:
                                            dicoDup[Name]['scrF']['nbBlast']+=1
                                            dicoDup[Name]['scrF']['initial_SCposRead'].append(SCposRead)
                                            if send>dicoDup[Name]['scrF']['limit']: dicoDup[Name]['scrF']['limit'] = send
                                        else: dicoDup[Name] = { 'scrF':{'nbBlast':1,'limit':send,'initial_SCposRead':[SCposRead]}, 'scrR':{'nbBlast':0,'limit':99999,'initial_SCposRead':[]}, 'freqF':0.0, 'freqR':0.0 }
                                    # deletions
                                    else:
                                        if Name in dicoDel:
                                            dicoDel[Name]['scrF']['nbBlast']+=1
                                            dicoDel[Name]['scrF']['initial_SCposRead'].append(SCposRead)
                                            if send>dicoDel[Name]['scrF']['limit']: dicoDel[Name]['scrF']['limit'] = send
                                        else: dicoDel[Name] = { 'scrF':{'nbBlast':1,'limit':send,'initial_SCposRead':[SCposRead]}, 'scrR':{'nbBlast':0,'limit':99999,'initial_SCposRead':[]}, 'freqF':0.0, 'freqR':0.0 } 
                        else:
                            Start = min(send-(SCmappedpart-qlen+alignLength)+1,dicoInit["dicoGbk"]['refLength']-1)
                            End = min(SCposRead,dicoInit["dicoGbk"]['refLength']-1)
                            Name = str(Start)+"-"+str(End)
                            # Filter deletions & duplications
                            if (Start<End and End-Start>=dicoInit['SCsize']) or (Start>End and Start-End>=dicoInit["mitosize"]):
                                # Determine if deletion include oriH or oriL
                                if Start>End: rangeDel = [*range(Start,16570), *range(1,End+1)]
                                else: rangeDel = range(Start,End+1)
                                intersec_oriH = list(set(dicoInit['range_oriH']) & set(rangeDel))
                                intersec_oriL = list(set(dicoInit['range_oriL']) & set(rangeDel))
                                # check exclude positions
                                boolExcludePosFind = False
                                for excludeRange in dicoInit['exclude_Breakpoint']:
                                    if Start in excludeRange or End in excludeRange:
                                        boolExcludePosFind = True
                                        break
                                # duplication
                                if boolExcludePosFind==False: 
                                    if len(intersec_oriH)>0 or len(intersec_oriL)>0:
                                        if Name in dicoDup:
                                            dicoDup[Name]['scrR']['nbBlast']+=1 
                                            dicoDup[Name]['scrR']['initial_SCposRead'].append(SCposRead)
                                            if sstart<dicoDup[Name]['scrR']['limit']: dicoDup[Name]['scrR']['limit'] = sstart
                                        else: dicoDup[Name] = { 'scrF':{'nbBlast':0,'limit':0,'initial_SCposRead':[]}, 'scrR':{'nbBlast':1,'limit':sstart,'initial_SCposRead':[SCposRead]}, 'freqF':0.0, 'freqR':0.0 }
                                    # deletion
                                    else:
                                        if Name in dicoDel:
                                            dicoDel[Name]['scrR']['nbBlast']+=1
                                            dicoDel[Name]['scrR']['initial_SCposRead'].append(SCposRead)
                                            if sstart<dicoDel[Name]['scrR']['limit']: dicoDel[Name]['scrR']['limit'] = sstart
                                        else: dicoDel[Name] = { 'scrF':{'nbBlast':0,'limit':0,'initial_SCposRead':[]}, 'scrR':{'nbBlast':1,'limit':sstart,'initial_SCposRead':[SCposRead]}, 'freqF':0.0, 'freqR':0.0 }                 
       
        # WRITE .json results
        JSON_del = open(pathBLASTjson_del, "w", encoding="utf8")
        JSON_dup = open(pathBLASTjson_dup, "w", encoding="utf8")
        JSON_del.write(json.dumps(dicoDel))
        JSON_dup.write(json.dumps(dicoDup))
        JSON_del.close()
        JSON_dup.close()
    except: 
        exc_type, exc_value, exc_traceback = sys.exc_info()
        lstError.append("SCBlastThread \""+titleBam+"\": "+str(exc_value)+" (line "+str(exc_traceback.tb_lineno)+")")

#=================================================================================================================================================#
#=================================================================================================================================================#
#=================================================================================================================================================#

def deletionPrediction(titleBam,dicoInit,lstError):
    try:
        pathSCjson = os.path.join(dicoInit['pathTmpDir'],titleBam+"_SC.json")
        pathBLASTjson_del = os.path.join(dicoInit['pathTmpDir'],titleBam+"_delBLAST.json")
        pathCumuljson_del = os.path.join(dicoInit['pathTmpDir'],titleBam+"_delcumul.json")
       
        #***** LOAD JSON files *****#
        dicoBam = load_json(pathSCjson)
        dicoDel = load_json(pathBLASTjson_del)
       
        #**** SHIFT Deletions *****#
        # Block one position and apply shift
        dico_shift_del = {}
        for Name in list(dicoDel.keys()):
            start = int(Name.split("-")[0])
            end = int(Name.split("-")[1])
            dico_index = len(dico_shift_del)
            dico_shift_del[dico_index] = [Name]
            for Name2 in list(dicoDel.keys()):
                if Name!=Name2:
                    start2 = int(Name2.split("-")[0])
                    end2 = int(Name2.split("-")[1])
                    # Same deletion start => shift end
                    if (start==start2 and (abs(end-end2)<=dicoInit["deldupShift"])) or (end==end2 and (abs(start-start2)<=dicoInit["deldupShift"])):
                        dico_shift_del[dico_index].append(Name2)
        # Merge set
        for key in list(dico_shift_del.keys()):
            for key2 in list(dico_shift_del.keys()):
                if key!=key2:
                    if len(set(dico_shift_del[key]).intersection(set(dico_shift_del[key2])))>0:
                        dico_shift_del[key] = set(dico_shift_del[key]).union(set(dico_shift_del[key2]))
                        dico_shift_del[key2] = set()
        # Merge to one maximal blast position
        for key in list(dico_shift_del.keys()):
            if len(dico_shift_del[key])>0:
                nbBlast_F = 0 ; nbBlast_R = 0
                lst_start_sc_fasta = [] ; lst_end_sc_fasta = []
                maxBlast_F = 0 ; maxBlast_R = 0
                maxBlast_start = "" ; maxBlast_end = ""
                limit_max_start = -1 ; limit_max_end = -1
                initial_SCposRead_F = [] ; initial_SCposRead_R = []
                # Browse deletion
                for Name in dico_shift_del[key]:
                    start = Name.split("-")[0]
                    end = Name.split("-")[1]
                    try: delnbblastF = dicoDel[Name]['scrF']['nbBlast']
                    except: delnbblastF = 0
                    try: delnbblastR = dicoDel[Name]['scrR']['nbBlast']
                    except: delnbblastR = 0
                    # Merge number of Blast
                    nbBlast_F+=delnbblastF
                    nbBlast_R+=delnbblastR
                    # Merge initial SC positions
                    try: initial_SCposRead_F.extend(dicoDel[Name]['scrF']['initial_SCposRead'])
                    except: pass
                    try: initial_SCposRead_R.extend(dicoDel[Name]['scrR']['initial_SCposRead'])
                    except: pass
                    # Find max blast positions for start and end
                    if delnbblastF>maxBlast_F:
                        maxBlast_F = delnbblastF
                        maxBlast_start = start
                        limit_max_start = dicoDel[Name]['scrF']['limit']
                    if delnbblastR>maxBlast_R:
                        maxBlast_R = delnbblastR
                        maxBlast_end = end
                        limit_max_end = dicoDel[Name]['scrR']['limit']
                    # Del entry
                    try : del(dicoDel[Name])
                    except: pass
                if maxBlast_start=="": maxBlast_start = Name.split("-")[0]
                if maxBlast_end=="": maxBlast_end = Name.split("-")[1]
                dicoDel[maxBlast_start+"-"+maxBlast_end] = { 'scrF':{'nbBlast':nbBlast_F,'limit':limit_max_start,'initial_SCposRead':initial_SCposRead_F},\
                                                             'scrR':{'nbBlast':nbBlast_R,'limit':limit_max_end,'initial_SCposRead':initial_SCposRead_R},\
                                                             'freqF':0.0, 'freqR':0.0 }

        #***** COMPUTE Frequencies del *****#
        for Name in list(dicoDel.keys()):
            # FILTERs 
            if dicoInit["bilateral"]==True and (dicoDel[Name]['scrF']['nbBlast']<dicoInit["minblast"] or dicoDel[Name]['scrR']['nbBlast']<dicoInit["minblast"]): del(dicoDel[Name])
            elif dicoInit["bilateral"]==False and dicoDel[Name]['scrF']['nbBlast']<dicoInit["minblast"] and dicoDel[Name]['scrR']['nbBlast']<dicoInit["minblast"]: del(dicoDel[Name])
            # Compute frequency
            else:
                start = int(Name.split("-")[0])
                end = int(Name.split("-")[1])
                nb_blast_F = float(dicoDel[Name]['scrF']['nbBlast'])
                nb_blast_R = float(dicoDel[Name]['scrR']['nbBlast'])
                nb_sc_fasta_F = 0 ; set_sc_fasta_pos_F = set()
                nb_sc_fasta_R = 0 ; set_sc_fasta_pos_R = set()
                if len(dicoDel[Name]['scrF']['initial_SCposRead'])==0: dicoDel[Name]['scrF']['initial_SCposRead'] = [start]
                if len(dicoDel[Name]['scrR']['initial_SCposRead'])==0: dicoDel[Name]['scrR']['initial_SCposRead'] = [end]
                # Check max total and soft-clipped reads in +-shifted pos
                lst_nb_sc_reads_F = []
                for init_pos in set(dicoDel[Name]['scrF']['initial_SCposRead']):
                    for i in range(max(1,init_pos-dicoInit["deldupShift"]),min(init_pos+dicoInit["deldupShift"],dicoInit["dicoGbk"]['refLength'])+1,1):
                        lst_nb_sc_reads_F.append(dicoBam[str(i)]['nb_sc_reads_F'])
                        if not i in set_sc_fasta_pos_F : nb_sc_fasta_F+=dicoBam[str(i)]['nb_sc_fasta_F'] ; set_sc_fasta_pos_F.add(i)
                lst_nb_sc_reads_R = []
                for init_pos in set(dicoDel[Name]['scrR']['initial_SCposRead']):
                    for i in range(max(1,init_pos-dicoInit["deldupShift"]),min(init_pos+dicoInit["deldupShift"],dicoInit["dicoGbk"]['refLength'])+1,1):
                        lst_nb_sc_reads_R.append(dicoBam[str(i)]['nb_sc_reads_R'])
                        if not i in set_sc_fasta_pos_R : nb_sc_fasta_R+=dicoBam[str(i)]['nb_sc_fasta_R'] ; set_sc_fasta_pos_R.add(i)
                # Max occurence start/end
                max_occ_start = max(dicoDel[Name]['scrF']['initial_SCposRead'],key=dicoDel[Name]['scrF']['initial_SCposRead'].count)
                max_occ_end = max(dicoDel[Name]['scrR']['initial_SCposRead'],key=dicoDel[Name]['scrR']['initial_SCposRead'].count)
                nb_reads_F = float(dicoBam[str(max_occ_start)]['nb_reads_F'])
                nb_reads_R = float(dicoBam[str(max_occ_end)]['nb_reads_R'])
                # Max number of sc reads
                if len(lst_nb_sc_reads_F)==0: nb_sc_reads_F = 0
                else: nb_sc_reads_F = float(max(lst_nb_sc_reads_F))
                if len(lst_nb_sc_reads_R)==0: nb_sc_reads_R = 0
                else: nb_sc_reads_R = float(max(lst_nb_sc_reads_R))
                # Frequencies
                if nb_sc_fasta_F==0: dicoDel[Name]['freqF'] = 0.0
                else: dicoDel[Name]['freqF'] = (nb_sc_reads_F/nb_reads_F) * (nb_blast_F/nb_sc_fasta_F) *100.0
                if nb_sc_fasta_R==0: dicoDel[Name]['freqR'] = 0.0
                else: dicoDel[Name]['freqR'] = (nb_sc_reads_R/nb_reads_R) * (nb_blast_R/nb_sc_fasta_R) *100.0
                dicoDel[Name]['depthF'] = nb_reads_F
                dicoDel[Name]['depthR'] = nb_reads_R

        #***** CUMULATIVE Frequency del *****#
        dicoCumulFreq_del = {}
        for delPos in list(dicoDel.keys()):
            start = int(delPos.split("-")[0])
            end = int(delPos.split("-")[1])
            if start<end: posRange = list(range(start,end+1,1))
            else: posRange = list(range(start,dicoInit["dicoGbk"]['refLength']+1,1)) ; posRange.extend(list(range(1,end+1,1)))
            for pos in posRange:
                if pos in dicoCumulFreq_del: dicoCumulFreq_del[pos]+=(dicoDel[delPos]['freqF']+dicoDel[delPos]['freqR'])/200.0
                else: dicoCumulFreq_del[pos] = (dicoDel[delPos]['freqF']+dicoDel[delPos]['freqR'])/200.0

        # WRITE .json results files
        JSON_del = open(pathBLASTjson_del, "w", encoding="utf8")
        JSON_del.write(json.dumps(dicoDel))
        JSON_del.close()
        JSON_del = open(pathCumuljson_del, "w", encoding="utf8")
        JSON_del.write(json.dumps(dicoCumulFreq_del))
        JSON_del.close()
    except: 
        exc_type, exc_value, exc_traceback = sys.exc_info()
        lstError.append("DelPredThread \""+titleBam+"\": "+str(exc_value)+" (line "+str(exc_traceback.tb_lineno)+")")

#=================================================================================================================================================#
#=================================================================================================================================================#
#=================================================================================================================================================#
def duplicationPrediction(titleBam,dicoInit,lstError):
    try:
        pathSCjson = os.path.join(dicoInit['pathTmpDir'],titleBam+"_SC.json")
        pathBLASTjson_dup = os.path.join(dicoInit['pathTmpDir'],titleBam+"_dupBLAST.json")
        pathCumuljson_dup = os.path.join(dicoInit['pathTmpDir'],titleBam+"_dupcumul.json")
       
        #***** LOAD JSON files *****#
        dicoBam = load_json(pathSCjson)
        dicoDup = load_json(pathBLASTjson_dup)
       
        #**** SHIFT Duplications *****#
        # Block one position and apply shift
        dico_shift_dup = {}
        for Name in list(dicoDup.keys()):
            start = int(Name.split("-")[0])
            end = int(Name.split("-")[1])
            dico_index = len(dico_shift_dup)
            dico_shift_dup[dico_index] = [Name]
            for Name2 in list(dicoDup.keys()):
                if Name!=Name2:
                    start2 = int(Name2.split("-")[0])
                    end2 = int(Name2.split("-")[1])
                    # Same duplication start => shift end
                    if (start==start2 and (abs(end-end2)<=dicoInit["deldupShift"])) or (end==end2 and (abs(start-start2)<=dicoInit["deldupShift"])):
                        dico_shift_dup[dico_index].append(Name2)
        # Merge set
        for key in list(dico_shift_dup.keys()):
            for key2 in list(dico_shift_dup.keys()):
                if key!=key2:
                    if len(set(dico_shift_dup[key]).intersection(set(dico_shift_dup[key2])))>0:
                        dico_shift_dup[key] = set(dico_shift_dup[key]).union(set(dico_shift_dup[key2]))
                        dico_shift_dup[key2] = set()
        # Merge to one maximal blast position
        for key in list(dico_shift_dup.keys()):
            if len(dico_shift_dup[key])>0:
                nbBlast_F = 0 ; nbBlast_R = 0
                lst_start_sc_fasta = [] ; lst_end_sc_fasta = []
                maxBlast_F = 0 ; maxBlast_R = 0
                maxBlast_start = "" ; maxBlast_end = ""
                limit_max_start = -1 ; limit_max_end = -1
                initial_SCposRead_F = [] ; initial_SCposRead_R = []
                # Browse duplication
                for Name in dico_shift_dup[key]:
                    start = Name.split("-")[0]
                    end = Name.split("-")[1]
                    try: dupnbblastF = dicoDup[Name]['scrF']['nbBlast']
                    except: dupnbblastF = 0
                    try: dupnbblastR = dicoDup[Name]['scrR']['nbBlast']
                    except: dupnbblastR = 0
                    # Merge number of Blast
                    nbBlast_F+=dupnbblastF
                    nbBlast_R+=dupnbblastR
                    # Merge initial SC positions
                    try: initial_SCposRead_F.extend(dicoDup[Name]['scrF']['initial_SCposRead'])
                    except: pass
                    try: initial_SCposRead_R.extend(dicoDup[Name]['scrR']['initial_SCposRead'])
                    except: pass
                    # Find max blast positions for start and end
                    if dupnbblastF>maxBlast_F:
                        maxBlast_F = dupnbblastF
                        maxBlast_start = start
                        limit_max_start = dicoDup[Name]['scrF']['limit']
                    if dupnbblastR>maxBlast_R:
                        maxBlast_R = dupnbblastR
                        maxBlast_end = end
                        limit_max_end = dicoDup[Name]['scrR']['limit']
                    # Dup entry
                    try : del(dicoDup[Name])
                    except: pass
                if maxBlast_start=="": maxBlast_start = Name.split("-")[0]
                if maxBlast_end=="": maxBlast_end = Name.split("-")[1]
                dicoDup[maxBlast_start+"-"+maxBlast_end] = { 'scrF':{'nbBlast':nbBlast_F,'limit':limit_max_start,'initial_SCposRead':initial_SCposRead_F},\
                                                             'scrR':{'nbBlast':nbBlast_R,'limit':limit_max_end,'initial_SCposRead':initial_SCposRead_R},\
                                                             'freqF':0.0, 'freqR':0.0 }

        #***** COMPUTE Frequencies dup *****#
        for Name in list(dicoDup.keys()):
            # FILTERs 
            if dicoInit["bilateral"]==True and (dicoDup[Name]['scrF']['nbBlast']<dicoInit["minblast"] or dicoDup[Name]['scrR']['nbBlast']<dicoInit["minblast"]): del(dicoDup[Name])
            elif dicoInit["bilateral"]==False and dicoDup[Name]['scrF']['nbBlast']<dicoInit["minblast"] and dicoDup[Name]['scrR']['nbBlast']<dicoInit["minblast"]: del(dicoDup[Name])
            # Compute frequency
            else:
                start = int(Name.split("-")[0])
                end = int(Name.split("-")[1])
                nb_blast_F = float(dicoDup[Name]['scrF']['nbBlast'])
                nb_blast_R = float(dicoDup[Name]['scrR']['nbBlast'])
                nb_sc_fasta_F = 0 ; set_sc_fasta_pos_F = set()
                nb_sc_fasta_R = 0 ; set_sc_fasta_pos_R = set()
                if len(dicoDup[Name]['scrF']['initial_SCposRead'])==0: dicoDup[Name]['scrF']['initial_SCposRead'] = [start]
                if len(dicoDup[Name]['scrR']['initial_SCposRead'])==0: dicoDup[Name]['scrR']['initial_SCposRead'] = [end]
                # Check max total and soft-clipped reads in +-shifted pos
                lst_nb_sc_reads_F = []
                for init_pos in set(dicoDup[Name]['scrF']['initial_SCposRead']):
                    for i in range(max(1,init_pos-dicoInit["deldupShift"]),min(init_pos+dicoInit["deldupShift"],dicoInit["dicoGbk"]['refLength'])+1,1):
                        lst_nb_sc_reads_F.append(dicoBam[str(i)]['nb_sc_reads_F'])
                        if not i in set_sc_fasta_pos_F : nb_sc_fasta_F+=dicoBam[str(i)]['nb_sc_fasta_F'] ; set_sc_fasta_pos_F.add(i)
                lst_nb_sc_reads_R = []
                for init_pos in set(dicoDup[Name]['scrR']['initial_SCposRead']):
                    for i in range(max(1,init_pos-dicoInit["deldupShift"]),min(init_pos+dicoInit["deldupShift"],dicoInit["dicoGbk"]['refLength'])+1,1):
                        lst_nb_sc_reads_R.append(dicoBam[str(i)]['nb_sc_reads_R'])
                        if not i in set_sc_fasta_pos_R : nb_sc_fasta_R+=dicoBam[str(i)]['nb_sc_fasta_R'] ; set_sc_fasta_pos_R.add(i)
                # Max occurence start/end
                max_occ_start = max(dicoDup[Name]['scrF']['initial_SCposRead'],key=dicoDup[Name]['scrF']['initial_SCposRead'].count)
                max_occ_end = max(dicoDup[Name]['scrR']['initial_SCposRead'],key=dicoDup[Name]['scrR']['initial_SCposRead'].count)
                nb_reads_F = float(dicoBam[str(max_occ_start)]['nb_reads_F'])
                nb_reads_R = float(dicoBam[str(max_occ_end)]['nb_reads_R'])
                # Max number of sc reads
                if len(lst_nb_sc_reads_F)==0: nb_sc_reads_F = 0
                else: nb_sc_reads_F = float(max(lst_nb_sc_reads_F))
                if len(lst_nb_sc_reads_R)==0: nb_sc_reads_R = 0
                else: nb_sc_reads_R = float(max(lst_nb_sc_reads_R))
                # Frequencies
                if nb_sc_fasta_F==0: dicoDup[Name]['freqF'] = 0.0
                else: dicoDup[Name]['freqF'] = (nb_sc_reads_F/nb_reads_F) * (nb_blast_F/nb_sc_fasta_F) *100.0
                if nb_sc_fasta_R==0: dicoDup[Name]['freqR'] = 0.0
                else: dicoDup[Name]['freqR'] = (nb_sc_reads_R/nb_reads_R) * (nb_blast_R/nb_sc_fasta_R) *100.0
                dicoDup[Name]['depthF'] = nb_reads_F
                dicoDup[Name]['depthR'] = nb_reads_R

        #***** CUMULATIVE Frequency dup*****#
        dicoCumulFreq_dup = {}
        for dupPos in list(dicoDup.keys()):
            start = int(dupPos.split("-")[0])
            end = int(dupPos.split("-")[1])
            if start<end: posRange = list(range(start,end+1,1))
            else: posRange = list(range(start,dicoInit["dicoGbk"]['refLength']+1,1)) ; posRange.extend(list(range(1,end+1,1)))
            for pos in posRange:
                if pos in dicoCumulFreq_dup: dicoCumulFreq_dup[pos]+=(dicoDup[dupPos]['freqF']+dicoDup[dupPos]['freqR'])/200.0
                else: dicoCumulFreq_dup[pos] = (dicoDup[dupPos]['freqF']+dicoDup[dupPos]['freqR'])/200.0

        # WRITE .json results files
        JSON_dup = open(pathBLASTjson_dup, "w", encoding="utf8")
        JSON_dup.write(json.dumps(dicoDup))
        JSON_dup.close()
        JSON_dup = open(pathCumuljson_dup, "w", encoding="utf8")
        JSON_dup.write(json.dumps(dicoCumulFreq_dup))
        JSON_dup.close()
    except: 
        exc_type, exc_value, exc_traceback = sys.exc_info()
        lstError.append("DupPredThread \""+titleBam+"\": "+str(exc_value)+" (line "+str(exc_traceback.tb_lineno)+")")

#=================================================================================================================================================#
#=================================================================================================================================================#
#=================================================================================================================================================#

def create_results_table_del(dicoInit,lstError):
    # Open output files
    pathOutDel = os.path.join(dicoInit['pathOutputDir'],"eKLIPse_deletions.csv")
    pathOutGenes1 = os.path.join(dicoInit['pathOutputDir'],"eKLIPse_genes_del.csv")
    OUTDEL = open(pathOutDel,'w')
    OUTGENES1 = open(pathOutGenes1,'w')
    # Write headers
    headerDel = "\"Title\";\"5' breakpoint\";\"3' breakpoint\";\"Freq\";\"Freq For\";\"Freq Rev\";\"5' Blast\";\"3' Blast\";\"5' Depth\";\"3' Depth\";\"Repetition\"\n"
    OUTDEL.write(headerDel)
    headerGenes = "\"Gene\";\"Start\";\"End\";\"Type\""
    for titleBam in list(dicoInit["dicoBam"].keys()): headerGenes = headerGenes+",\""+titleBam+"\""
    OUTGENES1.write(headerGenes+"\n")

    #***** BROWSE input alignments *****#
    dico_max_gene1 = {}
    for titleBam in list(dicoInit["dicoBam"].keys()):
        # Load json results files
        dicoDel = load_json(os.path.join(dicoInit['pathTmpDir'],titleBam+"_delBLAST.json"))
        dicoCumulFreq_del = load_json(os.path.join(dicoInit['pathTmpDir'],titleBam+"_delcumul.json"))

        #***** DELETIONS Results File *****#
        # Sort deletion by deletion start and stop
        dicoSortDel = {}
        for deletion in list(dicoDel.keys()):
            start = int(deletion.split("-")[0])
            end = int(deletion.split("-")[1])
            if start in dicoSortDel: dicoSortDel[start][end] = deletion
            else: dicoSortDel[start] = { end:deletion }
        lst_start = list(dicoSortDel.keys()) ; lst_start.sort()
        # Browse sorted deletions
        for start in lst_start:
            lst_end = list(dicoSortDel[start].keys()) ; lst_end.sort()
            for end in lst_end:
                # Search repetition (right)
                repetition_right = "" ; cpt_right = 0
                while dicoInit["dicoGbk"]['refSeq'][start+cpt_right]==dicoInit["dicoGbk"]['refSeq'][end-1+cpt_right]:
                    repetition_right = repetition_right+dicoInit["dicoGbk"]['refSeq'][start+cpt_right]
                    cpt_right+=1
                # Search repetition (right)
                repetition_left = "" ; cpt_left = 0
                while dicoInit["dicoGbk"]['refSeq'][start-2-cpt_left]==dicoInit["dicoGbk"]['refSeq'][end-1-cpt_left]:
                    repetition_left = repetition_left+dicoInit["dicoGbk"]['refSeq'][start-1-cpt_left]
                    cpt_left+=1
                repetition_left = repetition_left[::-1]
                # Keep best one
                if cpt_right==cpt_left==0: repetition = "none"
                elif cpt_right>=cpt_left:
                    repetition = str(start+1)+"-"+repetition_right+"-"+str(start+cpt_right)+" <> "+str(end)+"-"+repetition_right+"-"+str(end-1+cpt_right)
                else:
                    repetition = str(start-cpt_left)+"-"+repetition_left+"-"+str(start-1)+" <> "+str(end+1-cpt_left)+"-"+repetition_left+"-"+str(end) 
                # Write deletion line
                deletion = dicoSortDel[start][end]
                delPercent = ((dicoDel[deletion]['freqF']+dicoDel[deletion]['freqR'])/2.0)
                ToWrite = "\""+titleBam+"\";\""+str(start)+"\";\""+str(end)+"\";\""+str(delPercent).replace(".",",")+\
                          "\";\""+str(dicoDel[deletion]['freqF']).replace(".",",")+"\";\""+str(dicoDel[deletion]['freqR']).replace(".",",")+\
                          "\";\""+str(dicoDel[deletion]['scrF']['nbBlast'])+"\";\""+str(dicoDel[deletion]['scrR']['nbBlast'])+"\";\""+str(dicoDel[deletion]['depthF'])+"\";\""+str(dicoDel[deletion]['depthR'])+"\";\""+repetition+"\"\n"
                OUTDEL.write(ToWrite)

        #***** GENES max per genes hashtable *****#
        for features in dicoInit["dicoGbk"]['lstGene']:
            try: dico_max_gene1[features[0]][titleBam] = 0.0
            except:  dico_max_gene1[features[0]] = { titleBam:0.0 }
            for pos in list(dicoCumulFreq_del.keys()):
                if int(pos)>=features[1] and int(pos)<=features[2]: dico_max_gene1[features[0]][titleBam] = max(dico_max_gene1[features[0]][titleBam],dicoCumulFreq_del[pos])
            
    #***** GENES Results File *****#
    for features in dicoInit["dicoGbk"]['lstGene']:
        ToWrite = "\""+features[0]+"\";\""+str(features[1])+"\";\""+str(features[2])+"\";\""+features[3]+"\""
        for titleBam in list(dicoInit["dicoBam"].keys()):
            ToWrite = ToWrite+";\""+str(dico_max_gene1[features[0]][titleBam]*100.0).replace(".",",")+"\""
        OUTGENES1.write(ToWrite+"\n")

    # Close Files
    OUTDEL.close()
    OUTGENES1.close()

#==================================================================================================================================================================================
#==================================================================================================================================================================================
def create_results_table_dup(dicoInit,lstError):
    # Open output files
    pathOutDup = os.path.join(dicoInit['pathOutputDir'],"eKLIPse_duplications.csv")
    pathOutGenes2 = os.path.join(dicoInit['pathOutputDir'],"eKLIPse_genes_dup.csv")
    OUTDUP = open(pathOutDup,'w')
    OUTGENES2 = open(pathOutGenes2,'w')
    # Write headers
    headerDup = "\"Title\";\"5' breakpoint\";\"3' breakpoint\";\"Freq\";\"Freq For\";\"Freq Rev\";\"5' Blast\";\"3' Blast\";\"5' Depth\";\"3' Depth\";\"Repetition\"\n"
    OUTDUP.write(headerDup)
    headerGenes = "\"Gene\";\"Start\";\"End\";\"Type\""
    for titleBam in list(dicoInit["dicoBam"].keys()): headerGenes = headerGenes+",\""+titleBam+"\""
    OUTGENES2.write(headerGenes+"\n")

    #***** BROWSE input alignments *****#
    dico_max_gene2 = {}
    for titleBam in list(dicoInit["dicoBam"].keys()):
        # Load json results files
        dicoDup = load_json(os.path.join(dicoInit['pathTmpDir'],titleBam+"_dupBLAST.json"))
        dicoCumulFreq_dup = load_json(os.path.join(dicoInit['pathTmpDir'],titleBam+"_dupcumul.json"))

        #***** DELETIONS Results File *****#
        # Sort duplication by duplication start and stop
        dicoSortDup = {}
        for duplication in list(dicoDup.keys()):
            start = int(duplication.split("-")[0])
            end = int(duplication.split("-")[1])
            if start in dicoSortDup: dicoSortDup[start][end] = duplication
            else: dicoSortDup[start] = { end:duplication }
        lst_start = list(dicoSortDup.keys()) ; lst_start.sort()
        # Browse sorted duplications
        for start in lst_start:
            lst_end = list(dicoSortDup[start].keys()) ; lst_end.sort()
            for end in lst_end:
                # Search repetition (right)
                repetition_right = "" ; cpt_right = 0
                while dicoInit["dicoGbk"]['refSeq'][start+cpt_right]==dicoInit["dicoGbk"]['refSeq'][end-1+cpt_right]:
                    repetition_right = repetition_right+dicoInit["dicoGbk"]['refSeq'][start+cpt_right]
                    cpt_right+=1
                # Search repetition (right)
                repetition_left = "" ; cpt_left = 0
                while dicoInit["dicoGbk"]['refSeq'][start-2-cpt_left]==dicoInit["dicoGbk"]['refSeq'][end-1-cpt_left]:
                    repetition_left = repetition_left+dicoInit["dicoGbk"]['refSeq'][start-1-cpt_left]
                    cpt_left+=1
                repetition_left = repetition_left[::-1]
                # Keep best one
                if cpt_right==cpt_left==0: repetition = "none"
                elif cpt_right>=cpt_left:
                    repetition = str(start+1)+"-"+repetition_right+"-"+str(start+cpt_right)+" <> "+str(end)+"-"+repetition_right+"-"+str(end-1+cpt_right)
                else:
                    repetition = str(start-cpt_left)+"-"+repetition_left+"-"+str(start-1)+" <> "+str(end+1-cpt_left)+"-"+repetition_left+"-"+str(end) 
                # Write duplication line
                duplication = dicoSortDup[start][end]
                dupPercent = ((dicoDup[duplication]['freqF']+dicoDup[duplication]['freqR'])/2.0)
                ToWrite = "\""+titleBam+"\";\""+str(start)+"\";\""+str(end)+"\";\""+str(dupPercent).replace(".",",")+\
                          "\";\""+str(dicoDup[duplication]['freqF']).replace(".",",")+"\";\""+str(dicoDup[duplication]['freqR']).replace(".",",")+\
                          "\";\""+str(dicoDup[duplication]['scrF']['nbBlast'])+"\";\""+str(dicoDup[duplication]['scrR']['nbBlast'])+"\";\""+str(dicoDup[duplication]['depthF'])+"\";\""+str(dicoDup[duplication]['depthR'])+"\";\""+repetition+"\"\n"
                OUTDUP.write(ToWrite) 

        #***** GENES max per genes hashtable *****#
        for features in dicoInit["dicoGbk"]['lstGene']:
            try: dico_max_gene2[features[0]][titleBam] = 0.0
            except:  dico_max_gene2[features[0]] = { titleBam:0.0 }
            for pos in list(dicoCumulFreq_dup.keys()):
                if int(pos)>=features[1] and int(pos)<=features[2]: dico_max_gene2[features[0]][titleBam] = max(dico_max_gene2[features[0]][titleBam],dicoCumulFreq_dup[pos])
            
    #***** GENES Results File *****#
    for features in dicoInit["dicoGbk"]['lstGene']:
        ToWrite = "\""+features[0]+"\";\""+str(features[1])+"\";\""+str(features[2])+"\";\""+features[3]+"\""
        for titleBam in list(dicoInit["dicoBam"].keys()):
            ToWrite = ToWrite+";\""+str(dico_max_gene2[features[0]][titleBam]*100.0).replace(".",",")+"\""
        OUTGENES2.write(ToWrite+"\n")

    # Close Files
    OUTDUP.close()
    OUTGENES2.close()