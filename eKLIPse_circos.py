import pdfkit
import pdfcrowd
import time
import sys
import os
import shutil
from eKLIPse_fct import *



#=================================================================================================================================================#
#=================================================================================================================================================#
#=================================================================================================================================================#

def circosConf(titleBam,dicoInit,lstError):
    try:
        #***** PATHS *****#
        # Main conf Templates
        pathConfTemplate = os.path.join(dicoInit['pathDataDir'],"circos_template","circos.conf") 
        # Copy static conf files
        shutil.copy(os.path.join(dicoInit['pathDataDir'],"circos_template","ideogram.conf"),os.path.join(dicoInit['pathTmpDir'],"ideogram.conf"))
        shutil.copy(os.path.join(dicoInit['pathDataDir'],"circos_template","ticks.conf"),os.path.join(dicoInit['pathTmpDir'],"ticks.conf"))
        shutil.copy(os.path.join(dicoInit['pathDataDir'],"circos_template","bands.conf"),os.path.join(dicoInit['pathTmpDir'],"bands.conf"))
        # Template created from gbk input file
        pathKaryotypeTemplate = os.path.join(dicoInit['pathTmpDir'],titleBam+"_karyotype.colors.mt.txt")
        pathTextBands = os.path.join(dicoInit['pathTmpDir'],titleBam+"_text.bands.txt")
        TEMPLATE_KARYOTYPE = open(pathKaryotypeTemplate,'w')
        TEMPLATE_TEXTBANDS = open(pathTextBands,'w')
        TEMPLATE_KARYOTYPE.write("chr - hsM "+titleBam+" 0 "+str(dicoInit["dicoGbk"]['refLength']-1)+" chrM\n")
        cpt = 1 ; previous_end = 0 ; previous_color = ""
        for gene in dicoInit["dicoGbk"]['lstGene']:
            # Assign color to gene type
            if gene[3]=="trna":
                if previous_color=="orange": color = "dorange"
                else: color = "orange"
            elif gene[3]=="rrna":
                if previous_color=="purple": color = "vdpurple"
                else: color = "purple"
            else:
                if previous_color=="green": color = "vdgreen"
                else: color = "green"
            previous_color = color
            # Create karyotype and bands lines
            if previous_end<gene[1]: TEMPLATE_KARYOTYPE.write("band hsM band"+str(cpt)+" band"+str(cpt)+" "+str(previous_end)+" "+str(gene[1])+" vdgrey\n") ; cpt+=1
            if previous_end>gene[1]: TEMPLATE_KARYOTYPE.write("band hsM band"+str(cpt)+" band"+str(cpt)+" "+str(previous_end+1)+" "+str(gene[2])+" "+color+"\n")
            else: TEMPLATE_KARYOTYPE.write("band hsM band"+str(cpt)+" band"+str(cpt)+" "+str(gene[1])+" "+str(gene[2])+" "+color+"\n")
            TEMPLATE_TEXTBANDS.write("hsM "+str(gene[1])+" "+str(gene[2])+" "+gene[0]+"\n")
            cpt+=1 ; previous_end = gene[2]
        TEMPLATE_KARYOTYPE.write("band hsM band"+str(cpt)+" band"+str(cpt)+" "+str(previous_end+1)+" "+str(dicoInit["dicoGbk"]['refLength']-1)+" vdgrey\n")
        # For human reference display replications origins
        if dicoInit["dicoGbk"]['refName']=="NC_012920":
            TEMPLATE_TEXTBANDS.write("hsM 407 408 *******OH\nhsM 5747 5748 *******OL")
        TEMPLATE_KARYOTYPE.close()
        TEMPLATE_TEXTBANDS.close()

        # CurrenBAM specific files
        pathConf = os.path.join(dicoInit['pathTmpDir'],titleBam+"_circos.conf")
        pathCov = os.path.join(dicoInit["pathTmpDir"],titleBam+".cov")
        pathWarningCov = os.path.join(dicoInit['pathTmpDir'],titleBam+"_warning.cov")
        pathMeanCov = os.path.join(dicoInit['pathTmpDir'],"mean.cov")
        pathSCcircos = os.path.join(dicoInit['pathTmpDir'],titleBam+".sc")
        pathSCblastCircosDel = os.path.join(dicoInit['pathTmpDir'],titleBam+"_blastCircosDel.txt")
        pathSCblastCircosDel1 = os.path.join(dicoInit['pathTmpDir'],titleBam+"_blastCircosDel1.txt")
        pathSCblastCircosDel2 = os.path.join(dicoInit['pathTmpDir'],titleBam+"_blastCircosDel2.txt")
        pathSCblastCircosDel3 = os.path.join(dicoInit['pathTmpDir'],titleBam+"_blastCircosDel3.txt")
        pathSCblastCircosDup = os.path.join(dicoInit['pathTmpDir'],titleBam+"_blastCircosDup.txt")
        pathSCblastCircosDup1 = os.path.join(dicoInit['pathTmpDir'],titleBam+"_blastCircosDup1.txt")
        pathSCblastCircosDup2 = os.path.join(dicoInit['pathTmpDir'],titleBam+"_blastCircosDup2.txt")
        pathSCblastCircosDup3 = os.path.join(dicoInit['pathTmpDir'],titleBam+"_blastCircosDup3.txt")
        nbLineBlastLinkDel = 0 ; nbLineBlastLinkDel1 = 0 ; nbLineBlastLinkDel2 = 0 ; nbLineBlastLinkDel3 = 0
        nbLineBlastLinkDup = 0 ; nbLineBlastLinkDup1 = 0 ; nbLineBlastLinkDup2 = 0 ; nbLineBlastLinkDup3 = 0
        pathDelCumul = os.path.join(dicoInit['pathTmpDir'],titleBam+"_cumulFreqDel.txt")
        pathDupCumul = os.path.join(dicoInit['pathTmpDir'],titleBam+"_cumulFreqDup.txt")
        pathExcludeBreakpoint = os.path.join(dicoInit['pathTmpDir'],titleBam+"_excludebreakpoint.txt")

        #***** LOAD JSON files *****#
        dicoBam = load_json(os.path.join(dicoInit['pathTmpDir'],titleBam+"_SC.json"))
        dicoDel = load_json(os.path.join(dicoInit['pathTmpDir'],titleBam+"_delBLAST.json"))
        dicoDup = load_json(os.path.join(dicoInit['pathTmpDir'],titleBam+"_dupBLAST.json"))
        #***** COVERAGE (sample) *****#
        COV = open(pathCov,'w')
        for pos in range(0,dicoInit["dicoGbk"]['refLength']-2,1):
            if str(pos+1) in dicoBam:
                nbReadTotReal = dicoBam[str(pos+1)]['nb_reads_F']+dicoBam[str(pos+1)]['nb_reads_R']
                nbReadTotLimit = min(nbReadTotReal,dicoInit['max_cov'])
                ToWrite = "hsM\t"+str(pos+1)+"\t"+str(pos+2)+"\t"+str(nbReadTotLimit)+"\n"
            else: ToWrite = "hsM\t"+str(pos+1)+"\t"+str(pos+2)+"\t0\n"
            COV.write(ToWrite)
        COV.close() 
        #***** warning_cov *****#
        WarningCov = open(pathWarningCov,'w')
        for pos in range(0,dicoInit["dicoGbk"]['refLength']-2,1):
            if str(pos+1) in dicoBam :
                nbReadTotReal = dicoBam[str(pos+1)]['nb_reads_F']+dicoBam[str(pos+1)]['nb_reads_R']
                if nbReadTotReal < dicoInit['min_cov']:ToWrite = "hsM\t"+str(pos+1)+"\t"+str(pos+2)+"\t1\n"
                else: ToWrite = "hsM\t"+str(pos+1)+"\t"+str(pos+2)+"\t0\n"
            else: ToWrite = "hsM\t"+str(pos+1)+"\t"+str(pos+2)+"\t0\n"
            WarningCov.write(ToWrite)
        WarningCov.close()
        #***** SOFTCLIPPING (frequency peak) *****#
        CIRCOS = open(pathSCcircos,'w')
        for pos in range(dicoInit["dicoGbk"]['refLength']-1):
            freqSC = 0.0
            if str(pos+1) in dicoBam:
                if dicoBam[str(pos+1)]['nb_sc_reads_F']>dicoBam[str(pos+1)]['nb_sc_reads_R'] and dicoBam[str(pos+1)]['nb_reads_F']>0: freqSC = float(dicoBam[str(pos+1)]['nb_sc_reads_F'])/float(dicoBam[str(pos+1)]['nb_reads_F'])
                elif dicoBam[str(pos+1)]['nb_sc_reads_F']<dicoBam[str(pos+1)]['nb_sc_reads_R'] and dicoBam[str(pos+1)]['nb_reads_R']>0: freqSC = float(dicoBam[str(pos+1)]['nb_sc_reads_R'])/float(dicoBam[str(pos+1)]['nb_reads_R'])
            ToWrite = "hsM\t"+str(pos)+"\t"+str(pos+1)+"\t"+str(freqSC)+"\n"
            CIRCOS.write(ToWrite)
        CIRCOS.close()

        #***** BLAST Links del *****#
        CIRCOS = open(pathSCblastCircosDel,'w')
        CIRCOS1 = open(pathSCblastCircosDel1,'w')
        CIRCOS2 = open(pathSCblastCircosDel2,'w')
        CIRCOS3 = open(pathSCblastCircosDel3,'w')        
        for key in list(dicoDel.keys()):
            start = int(key.split("-")[0])
            end = int(key.split("-")[1])
            meanFreq = (dicoDel[key]['freqF']+dicoDel[key]['freqR'])/2.0
            if meanFreq >= 10:
                CIRCOS.write("hsM\t"+str(dicoDel[key]['scrR']['limit'])+"\t"+str(start)+"\thsM\t"+str(end)+"\t"+str(dicoDel[key]['scrF']['limit'])+"\n")
                nbLineBlastLinkDel+=1
            elif meanFreq >= 1:
                CIRCOS1.write("hsM\t"+str(dicoDel[key]['scrR']['limit'])+"\t"+str(start)+"\thsM\t"+str(end)+"\t"+str(dicoDel[key]['scrF']['limit'])+"\n")
                nbLineBlastLinkDel1+=1
            elif meanFreq >= 0.1:
                CIRCOS2.write("hsM\t"+str(dicoDel[key]['scrR']['limit'])+"\t"+str(start)+"\thsM\t"+str(end)+"\t"+str(dicoDel[key]['scrF']['limit'])+"\n")
                nbLineBlastLinkDel2+=1
            else:
                CIRCOS3.write("hsM\t"+str(dicoDel[key]['scrR']['limit'])+"\t"+str(start)+"\thsM\t"+str(end)+"\t"+str(dicoDel[key]['scrF']['limit'])+"\n")
                nbLineBlastLinkDel3+=1                
        CIRCOS.close()
        CIRCOS1.close()
        CIRCOS2.close()
        CIRCOS3.close()

        #***** BLAST Links dup *****#
        CIRCOS = open(pathSCblastCircosDup,'w')
        CIRCOS1 = open(pathSCblastCircosDup1,'w')
        CIRCOS2 = open(pathSCblastCircosDup2,'w')
        CIRCOS3 = open(pathSCblastCircosDup3,'w')        
        for key in list(dicoDup.keys()):
            start = int(key.split("-")[0])
            end = int(key.split("-")[1])
            meanFreq = (dicoDup[key]['freqF']+dicoDup[key]['freqR'])/2.0
            if meanFreq >= 10:
                CIRCOS.write("hsM\t"+str(dicoDup[key]['scrR']['limit'])+"\t"+str(start)+"\thsM\t"+str(end)+"\t"+str(dicoDup[key]['scrF']['limit'])+"\n")
                nbLineBlastLinkDup+=1
            elif meanFreq >= 1:
                CIRCOS1.write("hsM\t"+str(dicoDup[key]['scrR']['limit'])+"\t"+str(start)+"\thsM\t"+str(end)+"\t"+str(dicoDup[key]['scrF']['limit'])+"\n")
                nbLineBlastLinkDup1+=1
            elif meanFreq >= 0.1:
                CIRCOS2.write("hsM\t"+str(dicoDup[key]['scrR']['limit'])+"\t"+str(start)+"\thsM\t"+str(end)+"\t"+str(dicoDup[key]['scrF']['limit'])+"\n")
                nbLineBlastLinkDup2+=1
            else:
                CIRCOS3.write("hsM\t"+str(dicoDup[key]['scrR']['limit'])+"\t"+str(start)+"\thsM\t"+str(end)+"\t"+str(dicoDup[key]['scrF']['limit'])+"\n")
                nbLineBlastLinkDup3+=1                
        CIRCOS.close()
        CIRCOS1.close()
        CIRCOS2.close()
        CIRCOS3.close()   
       
        #***** CUMULATIVE Frequency del *****#
        dicoCumulFreq_del = load_json(os.path.join(dicoInit['pathTmpDir'],titleBam+"_delcumul.json"))
        OUT = open(pathDelCumul,'w')
        for pos in range(dicoInit["dicoGbk"]['refLength']-1):
            if str(pos) in dicoCumulFreq_del:
                if dicoCumulFreq_del[str(pos)]>1.0: dicoCumulFreq_del[str(pos)] = 1.0
                OUT.write("hsM\t"+str(pos)+"\t"+str(pos+1)+"\t"+str(dicoCumulFreq_del[str(pos)])+"\n")
            else: OUT.write("hsM\t"+str(pos)+"\t"+str(pos+1)+"\t0\n")
        OUT.close()

        #***** CUMULATIVE Frequency dup *****#
        dicoCumulFreq_dup = load_json(os.path.join(dicoInit['pathTmpDir'],titleBam+"_dupcumul.json"))
        OUT = open(pathDupCumul,'w')
        for pos in range(dicoInit["dicoGbk"]['refLength']-1):
            if str(pos) in dicoCumulFreq_dup:
                if dicoCumulFreq_dup[str(pos)]>1.0: dicoCumulFreq_dup[str(pos)] = 1.0
                OUT.write("hsM\t"+str(pos)+"\t"+str(pos+1)+"\t"+str(dicoCumulFreq_dup[str(pos)])+"\n")
            else: OUT.write("hsM\t"+str(pos)+"\t"+str(pos+1)+"\t0\n")
        OUT.close()

        #***** EXCLUDE BREAPOINTS Visualization *****#
        OUT = open(pathExcludeBreakpoint,'w')
        for pos in range(dicoInit["dicoGbk"]['refLength']-1):
            excludePos = False
            for excludeRange in dicoInit['exclude_Breakpoint']:
                if pos in excludeRange:
                    excludePos = True
                    break
            if excludePos==True: OUT.write("hsM\t"+str(pos)+"\t"+str(pos+1)+"\t1\n")
            else: OUT.write("hsM\t"+str(pos)+"\t"+str(pos+1)+"\t0\n")
        OUT.close()

        #***** CREATE Main Circos Configuration File *****#
        IN = open(pathConfTemplate,'r')
        lst_lines = IN.read().split("\n")
        IN.close()
        OUT = open(pathConf,'w')
        for line in lst_lines:
            if line.__contains__("#KARYOTYPE"): new_line = line.replace("#KARYOTYPE",pathKaryotypeTemplate)
            elif line.__contains__("#TEXTBANDS"): new_line = line.replace("#TEXTBANDS",pathTextBands)
            elif line.__contains__("#BLASTLINKSDEL0"): new_line = line.replace("#BLASTLINKSDEL0",pathSCblastCircosDel)
            elif line.__contains__("#LINKSNUMBERDEL0"): new_line = line.replace("#LINKSNUMBERDEL0",str(nbLineBlastLinkDel)) 
            elif line.__contains__("#BLASTLINKSDEL1"): new_line = line.replace("#BLASTLINKSDEL1",pathSCblastCircosDel1)
            elif line.__contains__("#LINKSNUMBERDEL1"): new_line = line.replace("#LINKSNUMBERDEL1",str(nbLineBlastLinkDel1))
            elif line.__contains__("#BLASTLINKSDEL2"): new_line = line.replace("#BLASTLINKSDEL2",pathSCblastCircosDel2)
            elif line.__contains__("#LINKSNUMBERDEL2"): new_line = line.replace("#LINKSNUMBERDEL2",str(nbLineBlastLinkDel2))
            elif line.__contains__("#BLASTLINKSDEL3"): new_line = line.replace("#BLASTLINKSDEL3",pathSCblastCircosDel3)
            elif line.__contains__("#LINKSNUMBERDEL3"): new_line = line.replace("#LINKSNUMBERDEL3",str(nbLineBlastLinkDel3))
            elif line.__contains__("#BLASTLINKSDUP0"): new_line = line.replace("#BLASTLINKSDUP0",pathSCblastCircosDup)
            elif line.__contains__("#LINKSNUMBERDUP0"): new_line = line.replace("#LINKSNUMBERDUP0",str(nbLineBlastLinkDup)) 
            elif line.__contains__("#BLASTLINKSDUP1"): new_line = line.replace("#BLASTLINKSDUP1",pathSCblastCircosDup1)
            elif line.__contains__("#LINKSNUMBERDUP1"): new_line = line.replace("#LINKSNUMBERDUP1",str(nbLineBlastLinkDup1))
            elif line.__contains__("#BLASTLINKSDUP2"): new_line = line.replace("#BLASTLINKSDUP2",pathSCblastCircosDup2)
            elif line.__contains__("#LINKSNUMBERDUP2"): new_line = line.replace("#LINKSNUMBERDUP2",str(nbLineBlastLinkDup2))
            elif line.__contains__("#BLASTLINKSDUP3"): new_line = line.replace("#BLASTLINKSDUP3",pathSCblastCircosDup3)
            elif line.__contains__("#LINKSNUMBERDUP3"): new_line = line.replace("#LINKSNUMBERDUP3",str(nbLineBlastLinkDup3))        
            elif line.__contains__("#SCFILE"): new_line = line.replace("#SCFILE",pathSCcircos)
            elif line.__contains__("#DATAWarningCOV"): new_line = line.replace("#DATAWarningCOV",pathWarningCov)
            elif line.__contains__("#DATACOV"): new_line = line.replace("#DATACOV",pathCov) 
            elif line.__contains__("#DATAMEDIANE"): new_line = line.replace("#DATAMEDIANE",pathMeanCov)
            elif line.__contains__("#DATAFREQDEL"): new_line = line.replace("#DATAFREQDEL",pathDelCumul)
            elif line.__contains__("#DATAFREQDUP"): new_line = line.replace("#DATAFREQDUP",pathDupCumul)
            elif line.__contains__("#EXCLUDEBREAKPOINT"): new_line = line.replace("#EXCLUDEBREAKPOINT",pathExcludeBreakpoint)
            elif line.__contains__("#NAMEDIR"): new_line = line.replace("#NAMEDIR",dicoInit['pathOutputDir'])
            elif line.__contains__("#NAMEFILE"): new_line = line.replace("#NAMEFILE","eKLIPse_"+titleBam+".png")
            else: new_line = line
            OUT.write(new_line+"\n")
        OUT.close()
    except: 
        exc_type, exc_value, exc_traceback = sys.exc_info()
        lstError.append("ConfThread \""+titleBam+"\": "+str(exc_value)+" (line "+str(exc_traceback.tb_lineno)+")")

#=================================================================================================================================================#
#=================================================================================================================================================#
#=================================================================================================================================================#

def circosPlot(titleBam,dicoInit,lstError):
    try:
        pathConf = dicoInit['pathTmpDir']+"/"+titleBam+"_circos.conf"
        pathLog = os.path.join(dicoInit["pathTmpDir"],titleBam+"_circos.log")
        pathPng = os.path.join(dicoInit['pathOutputDir'],"eKLIPse_"+titleBam+".png")
        cmd = dicoInit['pathCircosBin']+" -nosvg -conf "+pathConf+" > "+pathLog+" 2>&1"
        os.system(cmd)
        if not os.path.isfile(pathPng):
            lstError.append("PlotThread \""+titleBam+"\": any output .png created (check log file)")
    except: 
        exc_type, exc_value, exc_traceback = sys.exc_info()
        lstError.append("PlotThread \""+titleBam+"\": "+str(exc_value)+" (line "+str(exc_traceback.tb_lineno)+")")

#=================================================================================================================================================#
#=================================================================================================================================================#
#=================================================================================================================================================#

    #***** CREATE HTML File *****#
    pathEklipseTemplate = os.path.join(dicoInit['pathDataDir'],"eklipse_template_html","template.html")
    # Copy static conf files
    shutil.copy(os.path.join(dicoInit['pathDataDir'],"eklipse_template_html","copyright_icon.png"),os.path.join(dicoInit['pathTmpDir'],"copyright_icon.png"))
    shutil.copy(os.path.join(dicoInit['pathDataDir'],"eklipse_template_html","eklipse_white.png"),os.path.join(dicoInit['pathTmpDir'],"eklipse_white.png"))
    shutil.copy(os.path.join(dicoInit['pathDataDir'],"eklipse_template_html","github_icon.png"),os.path.join(dicoInit['pathTmpDir'],"github_icon.png"))
    shutil.copy(os.path.join(dicoInit['pathDataDir'],"eklipse_template_html","legend.png"),os.path.join(dicoInit['pathTmpDir'],"legend.png"))
    shutil.copy(os.path.join(dicoInit['pathDataDir'],"eklipse_template_html","mail_icon.png"),os.path.join(dicoInit['pathTmpDir'],"mail_icon.png"))
    shutil.copy(os.path.join(pathPng),os.path.join(dicoInit['pathTmpDir'],"eKLIPse_"+titleBam+".png"))
    pathConfHtml = os.path.join(dicoInit['pathTmpDir'],titleBam+"_eklipse.html")
    pathPngtmp = os.path.join(dicoInit['pathTmpDir'],"eKLIPse_"+titleBam+".png")
    format_time = time.strftime('%d/%m/%y %H:%M:%S',time.localtime(dicoInit['startTime']))
    INPUT = open(pathEklipseTemplate,'r')
    lst_lines = INPUT.read().split("\n")
    OUTPUT = open(pathConfHtml, "w")
    for line in lst_lines:
        if line.__contains__("#PNG"): new_line = line.replace("#PNG",pathPng)
        elif line.__contains__("#titleBam"): new_line = line.replace("#titleBam",titleBam)
        elif line.__contains__("#time") : new_line = line.replace("#time", format_time)
        else: new_line = line
        OUTPUT.write(new_line+"\n")
    OUTPUT.close()

    #**** Create PDF File *****#
    pathPdf = os.path.join(dicoInit['pathOutputDir'],"eKLIPse_"+titleBam+".pdf")
    pdfkit.from_file(pathConfHtml, pathPdf, options={'quiet': '',"enable-local-file-access": None})
